import csv
import unittest

from uk_postcodes import validate_postcode


class TestThreeFive(unittest.TestCase):

    def test_basic_postcodes(self):
        validation_data = ['EC1A 1BB', 'W1A 0AX', 'M1 1AE',
                           'B33 8TH', 'CR2 6XH', 'DN55 1PT']

        for postcode in validation_data:
            self.assertTrue(validate_postcode(postcode))

    def test_real_postcodes(self):

        with open('tests/ukpostcodes.csv', 'r') as csvfile:
            datareader = csv.DictReader(csvfile)

            for row in datareader:
                self.assertTrue(validate_postcode(row['postcode']))

if __name__ == '__main__':
    unittest.main()
