import re


SINGLE_DIGIT_DISTRICTS = ('BR', 'FY', 'HA', 'HD', 'HG', 'HR', 'HS', 'HX',
                          'JE', 'LD', 'SM', 'SR', 'WC', 'WN', 'ZE')
DOUBLE_DIGIT_DISTRICTS = ('AB', 'LL', 'SO')
ZERO_DISTRICTS = ('BL', 'BS', 'CM', 'CR', 'FY', 'HA', 'PR', 'SL', 'SS')
CENTRAL_LONDON_DISTRICTS = ('EC1', 'EC2', 'EC3', 'EC4', 'SW1', 'W1', 'WC1',
                            'WC2', 'E1', 'N1', 'NW1',  'SE1')

NOT_USED_FIRST_POSITION = 'QVX'
NOT_USED_SECOND_POSITION = 'IJZ'
USED_THIRD_POSITION = 'ABCDEFGHJKPSTUW'
USED_FOURTH_POSITION = 'ABEHMNPRVWXY'
UNIT_RESTRICTION = 'CIKMOV'


def validate_postcode(postcode):
    '''Return True if the input postcode is valid.'''

    match = re.fullmatch('(?P<area>[a-zA-Z]{1,2})(?P<district>\d\w?) (?P<sector>\d)(?P<unit>[a-zA-Z]{2})', postcode)
    if match is not None:
        area = match.group('area').upper()
        district = match.group('district').upper()
        outward_code = area + district
        unit = match.group('unit').upper()

        # Check single-digit area districts
        if len(district) != 1 and district.isdigit() and area in SINGLE_DIGIT_DISTRICTS:
            return False

        # Check duble-digit area districts
        if len(district) != 2 and district.isdigit() and area in DOUBLE_DIGIT_DISTRICTS:
            return False

        # Check area districts with 0
        if district == '0' and area not in ZERO_DISTRICTS:
            return False

        # Check central London single-digit area districts
        if outward_code[-1].isalpha():
            area_found = any([n in outward_code for n in CENTRAL_LONDON_DISTRICTS])
            if not area_found:
                return False

        # First position restrictions
        if area[0] in NOT_USED_FIRST_POSITION:
            return False

        # Second position restrictions
        if len(area) == 2 and area[1] in NOT_USED_SECOND_POSITION:
            return False

        # Third position restrictions
        if len(area) == 1 and len(district) == 2  and district[1].isalpha() and \
                district[1] not in USED_THIRD_POSITION:
            return False

        # Fourth position restrictions
        if len(area) == 2 and len(district) == 2  and district[1].isalpha() and \
                district[1] not in USED_FOURTH_POSITION:
            return False

        # Unit restrictions
        if any([n in unit for n in UNIT_RESTRICTION]):
            return False

        # Validated postcode
        return True

    return False
