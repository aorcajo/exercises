# UK postcode validate library

## Usage

```python
from uk_postcodes import validate_postcode

result = validate_postcode('SW1A 0AA')
```

## Running tests

```
python -m unittest tests.test_uk
```


NOTE: one of the tests is checking the library with a fragment of a real dataset. Original dataset was obtained from:
https://www.freemaptools.com/download-uk-postcode-lat-lng.htm
