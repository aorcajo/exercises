import unittest

from fizzbuzz import mult_3_5


class TestThreeFive(unittest.TestCase):

    def test_numbers(self):
        for i in range(1, 1000):
            result = mult_3_5(i)

            if i % 15 == 0:
                self.assertEqual('ThreeFive', result)
            elif i % 3 == 0:
                self.assertEqual('Three', result)
            elif i % 5 == 0:
                self.assertEqual('Five', result)
            else:
                self.assertEqual(str(i), result)


if __name__ == '__main__':
    unittest.main()
