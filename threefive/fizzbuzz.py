from __future__ import print_function


def mult_3_5(number):
    '''
    Returns 'Five' if the number is multiple of 5, 'Three' if it is multiple
    of 3 and 'ThreeFive' if it is multiple of both. Otherwise, returns the
    number in string format.

    Input: a number
    Output: string
    '''
    if not number % 3 and not number % 5:
        return 'ThreeFive'
    elif not number % 3:
        return 'Three'
    elif not number % 5:
        return 'Five'
    else:
        return str(number)

if '__main__' == __name__:
    [ print(mult_3_5(n)) for n in range(1, 101)]
